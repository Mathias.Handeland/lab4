package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import java.awt.geom.Rectangle2D;

// denne klassen har en metode som regner ut piksel-koordinatene for en gitt rute.

public class CellPositionToPixelConverter {
    Rectangle2D box; // beskriver innenfor hvilket område rutenettet skal tegnes
    GridDimension gd; // beskriver størrelsen til rutenettet rutene vil være en del av
    double margin; // beskriver hvor stor avstanden skal være mellom rutene

    // Konstruktør
    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        // Initaliser feltvariablene med argumentene som mottas i konstruktøren.
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    // en parameter av typen CellPosition (i figur under navgitt cp) og returtype Rectangle2D.
    public Rectangle2D getBoundsForCell(CellPosition cp) { // cp er cellposition
        // regner ut cellWidth og cellHeight, cellX og cellY
        double cellWidth = (box.getWidth() - ((gd.cols() + 1) * margin)) / gd.cols();
        double cellHeight = (box.getHeight() - ((gd.rows() + 1) * margin)) / gd.rows();

        double cellX = box.getX() + (margin * (cp.col() + 1)) + (cellWidth * cp.col());
        double cellY = box.getY() + (margin * (cp.row() + 1)) + (cellHeight * cp.row());
                
        Rectangle2D rectangle2d = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
        return rectangle2d; // Returverdien er et Rectangle2D -objekt.
    }
}
