package no.uib.inf101.gridview;

import javax.swing.JFrame;
import java.awt.Color;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;


public class Main {
  public static void main(String[] args) {
    // Opprett et GridView -objekt
    // Oppretter nytt ColorGrid-objekt med 3 rader og 4 kolonner
    ColorGrid colorGrid = new ColorGrid(3, 4);
    GridView gridView = new GridView(colorGrid); // SKRIV INN ARGUMENT
    
    // Sett fargen til å være Rød i hjørnet oppe til venstre (posisjon (0, 0))
    colorGrid.set(new CellPosition(0, 0), Color.RED);
    // Sett fargen til å være Blå i hjørnet oppe til høyre (posisjon (0, 3))
    colorGrid.set(new CellPosition(0, 3), Color.BLUE);
    // Sett fargen til å være Gul i hjørnet nede til venstre (posisjon (2, 0))
    colorGrid.set(new CellPosition(2, 0), Color.YELLOW);
    // Sett fargen til å være Grønn i hjørnet nede til høyre (posisjon (2, 3))
    colorGrid.set(new CellPosition(2, 3), Color.GREEN);



    // Opprett et JFrame -objekt, Dette objektet representerer «rammen» rundt vinduet, som inneholder tittelen og knappene for å lukke vinduet.
    JFrame frame = new JFrame();

    // Kall setContentPane -metoden på JFrame-objektet med GridView-objektet som argument
    frame.setContentPane(gridView);

    //  Kall setTitle, setDefaultCloseOperation, pack og setVisible på JFrame-objektet etter mønster fra kursnotatene om grafikk.
    frame.setTitle("INF101");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
