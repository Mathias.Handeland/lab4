package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Shape;

// denne klassen kan tegne et rutenett av farger.
public class GridView extends JPanel {
  // Instansvariabel
  IColorGrid iColorGrid;
  private static final double OUTERMARGIN = 30; // instansvariabel for avstand til kanten på vinduet
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY; // fargen til rektangelet på lerretet fylles med grå
  
  // Konstruktør
  public GridView(IColorGrid iColorGrid ) {
    this.setPreferredSize(new Dimension(400, 300)); // størrelsen på lerettet
    this.iColorGrid = iColorGrid; // Initialiser feltvariabelen med argumentet gitt til konstruktøren.
  }

  @Override // vi overskriver denne metoden slik at vi kan tegne vår egen tegning, og ikke bare et blankt vindu.
  public void paintComponent(Graphics g) {
    super.paintComponent(g); // kaller på supermetoden og tegner bakgrunnen til lerretet.
    Graphics2D g2 = (Graphics2D) g; // oppretter en Graphics2D -variabel fra g

  // gjør  et kall til:
    drawGrid(g2); // tegne et fullstendig rutenett, inkludert alle rammer og ruter (alt innenfor det grå området i illustrasjonen).
  }

  /*
  har som ansvar å tegne et fullstendig rutenett, inkludert alle rammer og ruter 
  (alt innenfor det grå området i illustrasjonen). 
  For å tegne selve rutene, kaller denne metoden på drawCells
  */ 
  // Siden metoden benytter instansmetoder kan metoden ikke være static. Siden metoden ikke skal benyttes av noen utenfor GridView -klassen, bør metoden være private.
  private void drawGrid(Graphics2D graphics2d) {
    // Et grått rektangel som alltid har 30px avstand til kantene på lerretet
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D myBox = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    graphics2d.setColor(MARGINCOLOR);
    graphics2d.fill(myBox);

    // Oppretter et nytt CellPositionToPixelConverter objekt
    CellPositionToPixelConverter cellConverter = new CellPositionToPixelConverter(myBox, iColorGrid, OUTERMARGIN);
    
    drawCells(graphics2d, iColorGrid, cellConverter);// legg inn argumenter; // kaller på denne metoden for å tegne selve rutene
  }

  /* parameterene til drawCells():
    et Graphics2D -objekt, lerretet rutene skal tegnes på
    et CellColorCollection -objekt, rutene som skal tegnes
    et CellPositionToPixelConverter -objekt som kan regne ut rutene sin posisjon
   */
  // siden drawCells ikke er avhengig av instansvariabler, bør metoden være static. Siden den ikke benyttes utenfor GridCell -klassen bør metoden være private.
  // tegne en samling av ruter. For hver rute regner denne metode ut hvor ruten skal være ved å kalle på hjelpemetoden
  private static void drawCells(Graphics2D canvas, CellColorCollection cellColor, CellPositionToPixelConverter cellConverter) {
    // itererer gjennom rutene i CellColorCollection -objektet, og tegne hver av dem på lerretet.
    for (CellColor cell : cellColor.getCells()) {
      if (cell.color() == null) {
        canvas.setColor(Color.DARK_GRAY);
        canvas.fill(cellConverter.getBoundsForCell(cell.cellPosition()));
      }
      else {
        canvas.setColor(cell.color());
        canvas.fill(cellConverter.getBoundsForCell(cell.cellPosition()));
      }
    }
  }
}
