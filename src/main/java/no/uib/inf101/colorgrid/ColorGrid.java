package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

// denne klassen representerer et rutenett av farger.
public class ColorGrid implements IColorGrid {
    // instansvariabler ( varer mellom metodekall )
    private int rows;
    private int cols;
    private Color color;

    // instansvariabler som kan lagre ting
    private List<List<Color>> grid; // en liste av en liste av farger, begge listene er tomme nå

  // Constructor
  public ColorGrid(int rows, int cols) {
      this.rows = rows; // Number of rows in the grid
      this.cols = cols; // Number of columns in the grid
      this.grid = new ArrayList<>();

      // opprette grid sitt utgangspunkt
      for (int i = 0; i < rows; i++) {
          this.grid.add(new ArrayList<Color>());
          for (int j = 0; j < cols; j++ ) {
              this.grid.get(i).add(color);
          }
      }
  }

  @Override
  public int rows() {
      return rows; // returnerer antall rader
  }

  @Override
  public int cols() {
      return cols; // returnerer alle kolonner
  }

  @Override
  public List<CellColor> getCells() {
  
      ArrayList<CellColor> cellColorList = new ArrayList<>(); // new list containing the GridCell objects in this collection
      // dobbel for løkke, først gjennom hver rad i grid som har typen List og inneholder Color
      for (int everyRow = 0; everyRow < this.rows; everyRow++) {
          for (int everyCol = 0; everyCol < this.cols; everyCol++) { // løkke gjennom hver kolonne som har typen Color i hver rad
            cellColorList.add(new CellColor(new CellPosition(everyRow, everyCol), grid.get(everyRow).get(everyCol)));
            // Vi legger ting inni den nye cellColorList
            // Først lages et nytt CellColor objekt, og her settes en ny CellPosition med posisjonen basert på rad og kolonne
            // Deretter blir fargen satt
        }
      }
        // return a list of all GridCell objects in this collection 
        return cellColorList; // returnerer en liste med fargene og posisjonen
  }

  @Override
  public Color get(CellPosition pos) throws IndexOutOfBoundsException {
    // throws IndexOutOfBoundsException if the position is out of bounds
   
      // pos the position
      int rowIndex = pos.row();
      int colIndex = pos.col();

      // Get the color of the cell at the given position.
      List<Color> row = this.grid.get(rowIndex); // henter ut en liste av de fargene som er på denne raden
      Color result = row.get(colIndex); // finner ut hvilken farge som er på den bestemte kolonne plassen pg legger det i result variabelen av typen Color
    
      return result; // return the color of the cell
  }

  @Override
  public void set(CellPosition pos, Color color) throws IndexOutOfBoundsException {
      // throws IndexOutOfBoundsException if the position is out of bounds

      // pos the position
      int row = pos.row(); 
      int col = pos.col();

      // Set the color of the cell at the given position.
      this.grid.get(row).set(col, color);
      // sier at: vi går inn i den bestemte raden og set endrer kolonnens verdi til verdien color
  } 
}
